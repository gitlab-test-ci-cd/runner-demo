from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# Sample route for the home page
@app.route('/')
def home():
    return render_template('index.html')

# Sample route for a form submission
@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        name = request.form['name']
        return redirect(url_for('greet', name=name))

# Route with a dynamic segment
@app.route('/greet/<name>')
def greet(name):
    return f'Hello, {name}! Welcome to Flask!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
